package projects

import (
	"context"
	"go.saastack.io/chaku/errors"
	company "go.saastack.io/company/pb"
	"go.saastack.io/idutil"
	"go.saastack.io/project/pb"
	"go.saastack.io/userinfo"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"


)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
)

type projectsServer struct {
	projectStore pb.ProjectStore
	parentServer pb.ParentServiceClient
	projectBLoC  pb.ProjectsServiceProjectServerBLoC
	*pb.ProjectsServiceProjectServerCrud
}

func NewProjectsServer(

	projectSt pb.ProjectStore,
	parentServer pb.ParentServiceClient,

) pb.ProjectsServer {
	r := &projectsServer{

		projectStore: projectSt,
		parentServer:   parentServer,

	}

	projectSC := pb.NewProjectsServiceProjectServerCrud(projectSt, r)
	r.ProjectsServiceProjectServerCrud = projectSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *projectsServer) CreateProjectBLoC(ctx context.Context, in *pb.CreateProjectRequest) error {

	if _, err := s.parentServer.ValidateParent(ctx, &pb.ValidateParentRequest{Id: in.GetParent()}); err != nil {
		return err
	}

	// Check if department title already not exists
	condition := pb.ProjectAnd{pb.ProjectParentEq{Parent: idutil.GetId(in.GetParent())}, pb.ProjectTitleEq{Title: in.GetProject().GetTitle()}}
	_, err := s.projectStore.GetProject(ctx, []string{string(pb.Project_Id)}, condition)
	if err != nil {

		if err == errors.ErrNotFound {
			return nil
		}
		return err
	}
	return errors.ErrObjIdExist

}

func (s *projectsServer) GetProjectBLoC(ctx context.Context, in *pb.GetProjectRequest) error {

	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil


}

func (s *projectsServer) UpdateProjectBLoC(ctx context.Context, in *pb.UpdateProjectRequest) error {
	//Validate view mask
	for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return errors.ErrInvalidField
		}
	}
	condition := pb.ProjectAnd{pb.ProjectParentEq{Parent: idutil.GetParent(in.GetProject().GetId())}, pb.ProjectTitleEq{Title: in.GetProject().GetTitle()}, pb.ProjectIdNotEq{Id: in.GetProject().GetId()}}
	_, err := s.projectStore.GetProject(ctx, []string{string(pb.Project_Id)}, condition)
	if err != nil {

		if err == errors.ErrNotFound {
			return nil
		}
		return err
	}
	return errors.ErrObjIdExist
}

func (s *projectsServer) DeleteProjectBLoC(ctx context.Context, in *pb.DeleteProjectRequest) error {
	return nil
}

func (s *projectsServer) BatchGetProjectBLoC(ctx context.Context, in *pb.BatchGetProjectRequest) error {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return errors.ErrInvalidField
		}
	}

	return nil

}

func (s *projectsServer) ListProjectBLoC(ctx context.Context, in *pb.ListProjectRequest) (pb.ProjectCondition, error) {
	//Validate view mask
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return nil, errors.ErrInvalidField
		}
	}

	if err := in.Validate(); err == nil	{
		return pb.ProjectFullParentEq{Parent: in.GetParent()}, nil
	}
	return pb.TrueCondition{}, nil


}
type parentServiceServer struct {
	comCli company.CompaniesClient
}

//NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(comCli company.CompaniesClient) pb.ParentServiceServer {
	return &parentServiceServer{
		comCli: comCli,
	}

}

// These functions are not implemented by CRUDGen, needed to be implemented
func (s *parentServiceServer) ValidateParent(ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {
	if skip := userinfo.SkipParent(ctx); !skip {
		if _, err := s.comCli.GetCompany(ctx, &company.GetCompanyRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}}); err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}
	}

	return &pb.ValidateParentResponse{Valid: true}, nil

}

func (s *parentServiceServer) BatchValidateParent(ctx context.Context, in *pb.BatchValidateParentRequest) (*pb.BatchValidateParentResponse, error) {
	if skip := userinfo.SkipParent(ctx); !skip {
		batchRes, err := s.comCli.BatchGetCompany(ctx, &company.BatchGetCompanyRequest{Ids: in.GetIds(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
		if err != nil {
			return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
		}
		for _, b := range batchRes.GetCompany() {
			if b == nil {
				return nil, status.Error(codes.FailedPrecondition, "Invalid Parent")
			}
		}
	}

	return &pb.BatchValidateParentResponse{Valid: true}, nil
}
