package projects

import (
	"context"
	"strings"

	"github.com/golang/protobuf/ptypes/empty"
	activityLog "go.saastack.io/activity-log"
	activityLogPb "go.saastack.io/activity-log/pb"
	"go.saastack.io/project/pb"
	"go.saastack.io/idutil"


)

type logsProjectsServer struct {
	pb.ProjectsServer
	actLogCli activityLogPb.ActivityLogsClient
	str       pb.ProjectStore
}

func NewLogsProjectsServer(
	a activityLogPb.ActivityLogsClient,
	s pb.ProjectsServer,
	str       pb.ProjectStore,
) pb.ProjectsServer {

	srv := &logsProjectsServer{s, a,str}

	return srv
}



func (s *logsProjectsServer) CreateProject(ctx context.Context, in *pb.CreateProjectRequest) (*pb.Project, error) {
	res, err := s.ProjectsServer.CreateProject(ctx, in)
	if err != nil {
		return nil, err
	}


	proj := &pb.Project{}
	if err = proj.Update(res, pb.ProjectFields(
		pb.Project_Title,
		pb.Project_Color,
	)); err != nil {
		return nil, err
	}

	/*
		Template: {{title}} {{color}}
	*/
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          in.GetParent(),
		Data:            proj,
		EventName:       ".saastack.projects.v1.Projects.CreateProject",
		ActivityId:      res.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil



}

func (s *logsProjectsServer) GetProject(ctx context.Context, in *pb.GetProjectRequest) (*pb.Project, error) {

	res, err := s.ProjectsServer.GetProject(ctx, in)
	if err != nil {
		return nil, err
	}

	return res, nil

}

func (s *logsProjectsServer) DeleteProject(ctx context.Context, in *pb.DeleteProjectRequest) (*empty.Empty, error) {

	proj, err := s.str.GetProject(ctx, []string{
		string(pb.Project_Title),
	}, pb.ProjectIdEq{Id: in.Id})
	if err != nil {
		return nil, err
	}

	res, err := s.ProjectsServer.DeleteProject(ctx, in)
	if err != nil {
		return nil, err
	}

	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(in.Id),
		Data:            proj,
		EventName:       ".saastack.projects.v1.Projects.DeleteProject",
		ActivityId:      in.Id,
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        map[string]string{},
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil

}

func (s *logsProjectsServer) UpdateProject(ctx context.Context, in *pb.UpdateProjectRequest) (*pb.Project, error) {

	proj, err := s.str.GetProject(ctx, []string{}, pb.ProjectIdEq{Id: in.GetProject().GetId()})
	if err != nil {
		return nil, err
	}

	res, err := s.ProjectsServer.UpdateProject(ctx, in)
	if err != nil {
		return nil, err
	}

	masks := pb.ProjectObjectCompare(proj, res, "")
	objMask := []string{}
	for _, oldMask := range masks {
		if strings.Contains(oldMask, "title") || strings.Contains(oldMask, "color") {
			continue
		}
		objMask = append(objMask, oldMask)
	}

	new := &pb.Project{}
	if err = new.Update(res, objMask); err != nil {
		return nil, err
	}
	old := &pb.Project{}
	if err = old.Update(proj, objMask); err != nil {
		return nil, err
	}

	updateProjectLog := map[string]*pb.Project{"new":new,"old":old}
	if err := activityLog.CreateActivityLogWrapper(ctx, activityLog.CreateActivityLogIn{
		Parent:          idutil.GetParent(res.GetId()),
		Data:            updateProjectLog,
		EventName:       ".saastack.projects.v1.Projects.UpdateProject",
		ActivityId:      res.GetId(),
		ActivityLogType: activityLogPb.ActivityLogType_UNSPECIFIED,
		Metadata:        make(map[string]string, 0),
	}, s.actLogCli); err != nil {
		return nil, err
	}

	return res, nil

}

func (s *logsProjectsServer) ListProject(ctx context.Context, in *pb.ListProjectRequest) (*pb.ListProjectResponse, error) {

	res, err := s.ProjectsServer.ListProject(ctx, in)
	if err != nil {
		return nil, err
	}


	return res, nil
}

func (s *logsProjectsServer) BatchGetProject(ctx context.Context, in *pb.BatchGetProjectRequest) (*pb.BatchGetProjectResponse, error) {

	res, err := s.ProjectsServer.BatchGetProject(ctx, in)
	if err != nil {
		return nil, err
	}


	return res, nil
}
